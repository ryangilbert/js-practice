// imports currently don't work with node 6, need to upgrade

import * as exports from 'export';

console.log('* as exports should contain all exports', exports);

import ModuleClass from 'export';

console.log('ModuleClass default export should be defined', !!ModuleClass);

import { ModuleFunction, ModuleConst } from 'export';

console.log('ModuleFunction and ModuleConst should be defined', !!ModuleFunction && !!ModuleConst)
