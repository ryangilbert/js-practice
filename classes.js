// construction

class Dog {

   constructor(name) {
      this.name = name;
   }

   fetch() {
      console.log('get the ball, ' + this.name + '!');
   }

   bark() {
      console.log('bark!');
   }

}

var spot = new Dog('Spot');

spot.fetch();
spot.bark();

// get/set

class Rectangle {

   constructor(height, width) {
      this.height = height;
      this.width = width;
      this.grams = 0;
   }

   calcArea() {
      return this.height * this.width;
   }

   get area() {
      return this.calcArea();
   }

   set weight(grams) {
      // setter must have different name than the property it's setting
      this.grams = grams;
      console.log('setting weight: ', this.grams);
   }

   get weight() {
      // getter must have different name than the property it's getting
      return this.grams;
   }

}

const square = new Rectangle(10, 10);

console.log('area of square: ', square.area);

square.weight = 100;

console.log('square weight: ', square.weight + ' grams');

// extends

class Fence extends Rectangle {

   constructor(height, width) {
      super(height, width);
   }

   get perimeter() {
      return this.calcPerimeter();
   }

   calcPerimeter() {
      return this.height * 2 + this.width * 2;
   }
}

const myFence = new Fence(10, 10);

console.log('fence perimeter', myFence.perimeter);

console.log('fence area', myFence.area);

class Utility {
   static throttle(callback) {
      setTimeout(callback, 500);
   }
}

Utility.throttle(function () {
   console.log('throttled');
});

function sides(numSides) {
   return function (target) {
      target.sides = numSides;
   };
}

@sides(6)
class Square {
   constructor(length, width, height) {
      this.length = length;
      this.width = width;
      this.height = height;
   }
}

var mySquare = new Square(1, 1, 1);

console.log(mySquare.sides);
