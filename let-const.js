var name = 'Ryan';

if (true) {
   let name = 'Bob';
}

console.log('name should be Ryan', name);

const surname = 'Gilbert';

console.log('const reassignment should throw TypeError');

try {
   surname = 'Smith';
}

catch (e) {
   console.log(e);
}
