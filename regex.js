var sentence = 'The quick brown fox jumps over the lazy dog';

// regex methods

console.log('regex.exec should return array: ', !!/fox/.exec(sentence)[0]);

console.log('regex.test should return true: ', /fox/.test(sentence));


// string methods

console.log('sentence.search should return index of "fox": ', sentence.search('fox') > -1);

console.log('sentence.replace should replace "fox" with "cat": ', sentence.replace('fox', 'cat').search('cat') > -1);

console.log('sentence.split(" ") should put put each word in an array: ', Array.isArray(sentence.split(' ')));


// special characters

console.log('/\/fox regex should should make "f" a special character and search for "ox": ', /\/fox/.test(sentence) == false);

console.log('/^the/i should be case insensitive and only match the first "The": ', /^the/i.exec(sentence)[0]) === "The";

console.log('/g$/ should only match the last "g" in the string "big dog": ', /g$/.exec("big dog").index === "big dog".length - 1);

console.log('/g+/ should match "doggg": ', /g+/.test("doggg"));

console.log('/doge?/ should match "dog" and "doge": ', /doge?/.test("dog") && /doge?/.test("doge"));

console.log('/.og/ should match "dog": ', /.og/.test("dog"));

console.log('see I.replace(/(see) (I)/, "$2 $1") should switch "see I" to "I see"', 'see I'.replace(/(see) (I)/, '$2 $1') == "I see");
