// IIFE

(function () {
   console.log('IIFE called');
})();


function defaultParams(x = 1) {
   console.log('default param: ', 1);
}

defaultParams();

var arrowFunction = () => {
   console.log('arrow function called');
};

arrowFunction();

var myParams = [2, 3];

function spreadOperator(x, ...params) {
   return [x, ...params];
}

console.log('spread operator should log "[ 1, 2, 3 ]"', spreadOperator(1, ...myParams));
