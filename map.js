var myMap = new Map([
   [1, 2],
   [3, 4]
]);

console.log(myMap);

console.log('mapSize', myMap.size);

myMap.delete(1);

console.log('delete first key: ', myMap);

console.log('get second key value, return 4: ', myMap.get(3));

console.log('add object 5/6 key/value pair: ', myMap.set(5, 6));

console.log('iterate map: ');

for (var [key, value] of myMap) {
   console.log(key, value);
}

console.log('map keys: ', myMap.keys());

console.log('for each map: ');

myMap.forEach(function (value, key) {
   console.log(key, value);
});
