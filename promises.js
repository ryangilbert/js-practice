function asyncFunction(mockErr) {
   return new Promise((resolve, reject) => {
      console.log('Mock async call');
      setTimeout(function () {
         if (mockErr) reject(mockErr);
         else resolve('Success');
      }, 1000);
   });
}

asyncFunction()
   .then(response => {
      console.log('Resolved response: ', response);
      return response;
   })
   .then(response => {
      console.log('Chained response: ', response);
   });

Promise.all([asyncFunction(), asyncFunction()]).then(response => {
   console.log(response);
   return response;
});

asyncFunction(new Error())
   .catch(err => {
      console.error('Caught error: ', err);
   });
