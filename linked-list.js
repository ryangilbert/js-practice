function Node(data, next) {
    this.data = data;
    this.next = next;
}

var n1 = new Node("Hello", null);
var n2 = new Node("World", n1);
var n3 = new Node("!", n2);

console.log(n3);

var start = n3;
var fast = start;
var slow = start;

while (fast.next !== null && fast.next.next !== null) {
    fast = fast.next.next;
    slow = slow.next;
}

var middle = slow.data;

console.log(middle);

