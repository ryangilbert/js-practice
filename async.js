function request(url, mockErr) {
   return new Promise((resolve, reject) => {
      setTimeout(function () {
         if (mockErr) reject(mockErr);
         else resolve('data');
      }, 1000);
   });
}

var mockAsync = async function (url, mockErr) {
   var data = await request(url, mockErr);
   return data;
};

mockAsync('url').then(result => {
   console.log('result:', result);
});

mockAsync('url', new Error('Error')).catch(err => {
   console.error(err);
});
