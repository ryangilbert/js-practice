var name = 'Ryan';

var greeting = `Hello, ${name}`;

console.log('greeting should display "Hello, Ryan":', greeting);

var responses = {
   morning: 'Good morning',
   afternoon: 'Good afternoon'
};

function response(time) {
   console.log('response should say "Good morning":', `${responses[time]}`);
}

response('morning');
