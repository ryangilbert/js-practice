"use strict";

//determine if # is integer

function isInt(num) {
    console.log(num % 1);
    return num % 1 === 0;
}

console.log(isInt(20));

// multiply (a)(b);
function multiply(a) {
    return function(b) {
        return a * b;
    };
}

console.log(multiply(2)(4));

//create a method
var arr = [1, 2, 3, 4, 5];

Array.prototype.average = function() {
    var sum = this.reduce(function(previous, current) {
        return previous + current;
    });
    
    var total = this.length;
    
    return sum / total;
};

var avg = arr.average();

console.log(avg);

// create a callback

function f1(arg, callback) {
    callback(arg);
}

f1('1', function(arg) {
   console.log(arg);
});

// floating point errors and rounding to 1 decimal place

var nums = 0.1 + 0.2;
var roundedSum = function() {
    console.log(nums);
    return nums.toFixed(1);
}();
console.log(roundedSum);



