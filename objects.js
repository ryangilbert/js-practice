const x = 1;
const y = 2;

const shorthandObject = { x, y };

console.log('shorthandObject:', shorthandObject);

function property() {
   return 'Property';
}

const computedProperties = {
   ['computed' + property()]: 'value'
};

console.log('computedProperties:', computedProperties);

var methodSyntax = {
   traditionalMethod: function () {},
   newMethod() {}
};
