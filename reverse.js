// FROM TECH INTERVIEW AT SOLUTION STREET

// write function that takes integer and returns factorial of that integer

function returnFactorial(int) {
    
    var factorial = int;

    if (!int) {
        return 1;
    }
    
    for (var i = 1; i < int; i++) {
        factorial = factorial * i;
    }

    return factorial;
}

//console.log(returnFactorial(5));

console.log(5 * 4 * 3 * 2 * 1);

// write function that takes in a string and return the reverse of the string w/o library

function reverseString(str) {

    var reversedString = "";
    
    for (var i = 0, len = str.length; i < len; i++) {
        reversedString += str[len - i - 1];
    }
    
    return reversedString;
    
}

console.log(reverseString('foo'));

