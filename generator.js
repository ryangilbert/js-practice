let fibonacci = {
   * [Symbol.iterator]() {
      let pre = 0,
         cur = 1;
      for (;;) {
         [pre, cur] = [cur, pre + cur];
         yield cur;
      }
   }
};

for (let n of fibonacci) {
   console.log(n);
   if (n > 1000)
      break;
}
