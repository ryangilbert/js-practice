// IIFE

for (var i = 0; i < 3; i++) {
  setTimeout(function(j) { 
  	return console.log(j); 
    }(i),1000 + i);
}

//closures

function createBase(num) {
    return function(second) {
        return num + second;
    };
}
var addSix = createBase(6);
addSix(10);
console.log(addSix(10));
console.log(addSix(21));

// private variables

function privateCounter() {
    var count =  0;
    return {
        addNum: function(num) {
            console.log('num: ', num);
            count += num;
            console.log(count);
        },
        get: function() {
            return count;
        }
    };
}

var c = privateCounter();
c.addNum(2);
console.log('get num: ', c.get());

