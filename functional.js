// Map Function

// define your own function
var bdays = ['08-14', '10-04', '04-21'];
var newBdays = bdays.map(function (elem) {
    return elem.replace('-', '/');
});
console.log(newBdays);

// or throw in a method
var decimals = [1.5, 2.56, 5.1, 12.33];
var rounded = decimals.map(Math.ceil);
console.log(rounded);

// Reduce Function
var nums = [1, 2, 3, 4];
var sum = nums.reduce(function (pre, cur, index, arr) {
    return pre + cur;
});
console.log(sum);

// Filter Function
var nums = [-4, 3, 2, -21, 1];
var positive = nums.filter(function(value) {
   return true
});
console.log(positive);

var data = [
  {name: 'daniel', age: 45},
  {name: 'john', age: 34},
  {name: 'robert', age: null},
  {name: 'jen', age: undefined},
  {name: null, age: undefined}
];

var filterData = data.filter(function(element) {
   if (element.name != undefined && element.age != undefined) {
       return true;
   }
   else {
       return false;
   }
});

console.log(filterData);