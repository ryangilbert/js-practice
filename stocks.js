var stockPrices = [44, 30, 24, 32, 35, 30, 40, 38, 15];

function stockPicker(prices) {

    var maxProfit = -1;
    var buy = 0;
    var sellPrice = 0;
    var swappingPrices = true;

    for (var i = 0, len = prices.length - 1; i < len; i++) {
        // next element in list
        sellPrice = prices[i + 1];
        
        if (swappingPrices) {
            buy = prices[i];
        }
        
        if (sellPrice < buy ) {
            swappingPrices = true;
            continue;
        }
        
        else {
            var tempProfit = sellPrice - buy;
            
            if (tempProfit > maxProfit) {
                maxProfit = tempProfit;
                swappingPrices = false;
            }
        }
    }
    
    return maxProfit;
}

stockPicker(stockPrices);
