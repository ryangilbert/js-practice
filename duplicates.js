// find duplicates with hashmap - O(n)

function findDuplicates(array) {
    
    var hashtable = [];
    var duplicates =  [];
    
    for (var i=0, len=array.length; i<len; i++) {
        var element = Math.abs(array[i]);
        
        if (hashtable[element] === undefined ) {
            hashtable[array[i].toString()] = true;
        }
        else {
            // duplicate
            duplicates.push(array[i]);
        }
    }
    return duplicates;
}

console.log(findDuplicates([1, 21, -4, 103, 21, 4, 1]));